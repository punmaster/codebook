new Phaser.Game({
    width: 683,// 1366
    height: 384,//768
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {y: 2000,},
            debug: true
        }
    },
    scene: {
        preload: function () {
            this.load.image('bg', './assets/platformer-bg.png')
            this.load.image('dot', './assets/dot.png')
            this.load.image('plat', './assets/platformer-platform.png')
        },
        create() {
            this.add.image(0,0, 'bg').setOrigin(0,0)
            dot = this.physics.add.sprite(100,100, 'dot')
            dot.setCollideWorldBounds(true)
            dot.setBounce(0)
            dot.setDrag(400)
            
            plats = this.physics.add.staticGroup()
            plats.create(300,-100,'plat')


            this.physics.add.collider(dot,plats)
            curs = this.input.keyboard.createCursorKeys ()
        },
        update() {
            if (curs.left.isDown){
                dot.setVelocityX(-150)
            } else if (curs.right.isDown) {
                dot.setVelocityX(150)
            } 
            if (dot.body.onFloor() && curs.space.isDown) {
                dot.setVelocityY(-800)
            }

        }
        
    }
})