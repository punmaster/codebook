let dot, k, jump, music, score, scoreText
const keys = 'LEFT,RIGHT,SPACE,UP,W,A,S,D,R'

const randint = lim => Math.floor(Math.random()*lim)
const randrng = (min,max) => randint(max-min+1)+min


class Main extends Phaser.Scene {
    init() {
        score = 0
    }
    preload() {
        this.load.image('bg', './assets/images/background_plt.png')
        this.load.image('dot', './assets/images/dot.png')
        this.load.image('bad', './assets/images/enemy.png')
        this.load.image('plat', './assets/images/platformer-platform.png')
        this.load.image('gameover','./assets/images/Gameover.png')
        this.load.image('coin', './assets/images/collectible.png')
        this.load.audio('music', './assets/sounds/music.wav')
        this.load.audio('collect', './assets/sounds/collect.wav')
        this.load.audio('die', './assets/sounds/die.wav')
        this.load.audio('jump', './assets/sounds/jump.wav')
    }

    create() {
        this.add.image(0, 0, 'bg').setOrigin(0, 0)
        dot = this.physics.add.sprite(100, 100, 'dot')

        dot.setCollideWorldBounds(true)
        dot.setBounce(0)
        dot.setDragX(2200)
        dot.setGravityY(2200)
        k = this.input.keyboard.addKeys(keys)

        const plat = this.physics.add.staticGroup()
        plat.create(300, 650, 'plat')
        plat.create(700, 650, 'plat')
        plat.create(500, 550,'plat')

        let c
        let coins = this.physics.add.staticGroup()
        
        const spawncoin = (x,y) =>{
            if (!x) x = randrng(280,530)
            if (!y) y = randrng (500,780)
            coins.create(x,y,'coin')
        }
        const coinfix = (c,p) => {
            c.destroy()
            spawncoin()
        }

        let enemy = this.physics.add.group()
        const spawnEnemy = () => {
            randint(1)
            let x = randint(game.config.width)
            let y = randint(game.config.height)
            let e = enemy.create(x,y, 'bad')
            e.setCollideWorldBounds(true)
            e.setVelocity(600)
            e.setBounce(1)
        }


        spawnEnemy()
        setInterval(spawnEnemy, 10000)
        
        music = this.sound.add('music',{volume: .08,loop: true,})
        let collect = this.sound.add('collect')
        jump = this.sound.add('jump',{volume: .07})
        let die = this.sound.add('die',{volume: .07})

        let font = {
            fontSize: '32px',
            fill: '#FFF',
        }
        scoreText = this.add.text(16,16,'Score: 0', font)

        for (let n=0; n<1; n++) spawncoin()
        
        const pickupcoin = (dot,c) => {
            collect.play()  
            score += 1
            scoreText.setText(`score: ${score}`)
            c.destroy()
            spawncoin()
        }
        const gameover = (d,e) => {
            this.physics.pause()
            this.add.image(0, 0, 'gameover').setOrigin(0, 0)
            music.stop()
            die.play()
        }


        this.physics.add.collider(dot,plat)
        this.physics.add.overlap(dot,coins,pickupcoin)
        this.physics.add.collider(coins,plat,coinfix)
        this.physics.add.collider(enemy,plat)
        this.physics.add.overlap(dot,enemy,gameover)
        
        music.play()

    }

    update() {
        if (k.LEFT.isDown){
            dot.setVelocityX(-250)
        } else if (k.RIGHT.isDown) {
            dot.setVelocityX(250)
        } 
    
    
        if (dot.body.onFloor() && k.SPACE.isDown) {
            dot.setVelocityY(-800)
            jump.play()
        }

        if (k.R.isDown){
            music.stop()
            this.scene.restart()
        }
    }
}

let game = new Phaser.Game({
    scene: Main,
    physics: { default: 'arcade' }
})