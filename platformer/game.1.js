let dot, plats,scoreText
const keys = 'LEFT,RIGHT,UP,SPACE,W,A,S,D'
let score = 0

function preload() {
    this.load.image('bg', './assets/images/background_plt.png')
    this.load.image('dot', './assets/images/dot.png')
    this.load.image('bad', '..assets/images/enemy.png')
    this.load.image('plat', './assets/images/platformer-platform.png')
    this.load.image('coin', './assets/images/collectible.png')
    this.load.audio('collect', './assets/sounds/collect.wav')
    this.load.audio('die', './assets/sounds/die.wav')
    this.load.audio('jump', './assets/sounds/jump.wav')
}

function create() {
    this.add.image(0,0, 'bg').setOrigin(0,0)
    dot = this.physics.add.sprite(100,100, 'dot')
    dot.setCollideWorldBounds(true)
    dot.setBounce(0)
    dot.setDragX(2200)
    dot.setGravityY(2200)

    
    plats = this.physics.add.staticGroup()
    plats.create(300,700,'plat')

    let collect = this.sound.add('collect')
    let jump = this.sound.add('jump')

    let c
    coins = this.physics.add.group()
    
    for (let x = 20; x<1000; x+=50) {
        let c = coins.create(x,600,'coin')
        c.setCollideWorldBounds(true)
        c.setGravityY(200)
    }

    let font = {
        fontSize: '32px',
        fill: '#FFF',
    }
    scoreText = this.add.text(16,16,'Score: 0', font)

    function collectCoin(dot,c) {
        c.destroy()
        score += 1 
        scoreText.setText(`score: ${score}`)
        collect.play()
        
    }

    this.physics.add.collider(dot,plats)
    this.physics.add.overlap(dot,coins,collectCoin)
    this.physics.add.collider(coins,plats)
    k = this.input.keyboard.addKeys(keys)
    
}

function update() {
    if (k.LEFT.isDown){
        dot.setVelocityX(-250)
    } else if (k.RIGHT.isDown) {
        dot.setVelocityX(250)
    } 


    if (dot.body.onFloor() && k.SPACE.isDown) {
        dot.setVelocityY(-800)
        jump.play()
    }
}

let config = {
   // width: 683,
   // height: 384,
    scene: {
        preload: preload,
        create: create,
        update: update
    },
    scene: { preload, create, update},
    physics: {default: 'arcade'},
}

new Phaser.Game(config)